/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var facetas = [];
var valoresFacetas = [];
var structFacetas = [];
var minimoGlobal = 0;
var maximoGlobal = 0;

function criarObjetoXMLHttpRequest() {
    var objXMLHttpRequest = null;
    if (window.XMLHttpRequest) {
        objXMLHttpRequest = new XMLHttpRequest();
    }
    else if (window.ActiveXObjet) {
        objXMLHttpRequest = new ActiveXObjet("Microsoft.XMLHTTP");
    }
    if (objXMLHttpRequest === null) {
        alert("Erro ao criar o objecto XMLHttpRequest Facetas!");
    }
    return objXMLHttpRequest;
}

function requestFacetas() {
    var resultObjetoXMLHttpRequest = criarObjetoXMLHttpRequest();
    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/facetas.php";
    resultObjetoXMLHttpRequest.open("GET", url, true);
    resultObjetoXMLHttpRequest.onreadystatechange = function () {
        atualizarPagina(resultObjetoXMLHttpRequest);
    };
    resultObjetoXMLHttpRequest.send(null);
}

function atualizarPagina(objetoXMLHttpRequest) {
    if (objetoXMLHttpRequest.readyState === 4 &&
            objetoXMLHttpRequest.status === 200) {
        var xmlDoc = objetoXMLHttpRequest.responseXML;
        var nosFacetas = xmlDoc.getElementsByTagName("faceta");
        for (var i = 0; i < nosFacetas.length; i++) {
            facetas[i] = nosFacetas[i].childNodes[0].nodeValue;
            structFacetas[i] = {};
            structFacetas[i]["nome"] = facetas[i];
            requestValoresFacetas(facetas[i], i);
        }
    }
}

function requestValoresFacetas(faceta, i) {
    var resultObjetoXMLHttpRequest = criarObjetoXMLHttpRequest();
    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/valoresFaceta.php?faceta=" + faceta;
    resultObjetoXMLHttpRequest.open("GET", url, true);
    resultObjetoXMLHttpRequest.onreadystatechange = function () {
        if (resultObjetoXMLHttpRequest.readyState === 4 &&
                resultObjetoXMLHttpRequest.status === 200) {
            var xmlDoc = resultObjetoXMLHttpRequest.responseText;
            valoresFacetas = JSON.parse(xmlDoc);
            requestTipoDadosFaceta(faceta, valoresFacetas, i);
        }
    };
    resultObjetoXMLHttpRequest.send(null);
}

function requestTipoDadosFaceta(faceta, valoresFacetas, i) {
    var resultObjetoXMLHttpRequest = criarObjetoXMLHttpRequest();
    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/tipoFaceta.php?faceta=" + faceta;
    resultObjetoXMLHttpRequest.open("GET", url, true);
    resultObjetoXMLHttpRequest.onreadystatechange = function () {
        if (resultObjetoXMLHttpRequest.readyState === 4 &&
                resultObjetoXMLHttpRequest.status === 200) {
            var xmlDoc = resultObjetoXMLHttpRequest.responseText;
            var tipoDados = JSON.parse(xmlDoc);
            var tipoDadosString = tipoDados["discreto"];
            if (tipoDadosString === "discreto") {
                structFacetas[i]["valoresFac"] = valoresFacetas;
                preencherValoresFacetas(faceta, valoresFacetas);
            } else {
                criarSlider(faceta);
            }
            //preencherValoresFacetas(faceta, valoresFacetas, tipoDadosString);
        }
    };
    resultObjetoXMLHttpRequest.send(null);
}

function preencherValoresFacetas(faceta, valoresFacetas) {
    var div0 = document.createElement("div");
    div0.className = "caixa";
    var paragr = document.createElement("p");
    //if(tipoDados === "discreto"){
    paragr.onclick = function () {
        for (var i = 0; i < valoresFacetas.length; i++) {
            var paragrafos = document.getElementById("paragr2" + valoresFacetas[i]);
            if (paragrafos !== null) {
                if (paragrafos.style.display === "block") {
                    paragrafos.style.display = "none";
                } else {
                    paragrafos.style.display = "block";
                }
            }
        }
    };
    var strong = document.createElement("strong");
    var titulo = document.createTextNode(camelCase(faceta, "_"));
    strong.appendChild(titulo);
    paragr.appendChild(strong);
    div0.appendChild(paragr);
    var paragr2 = document.createElement("p");
    for (var i = 0; i < valoresFacetas.length; i++) {
        if (valoresFacetas[i] !== "") {
            paragr2.id = "paragr2" + valoresFacetas[i];
            paragr2.style.display = "none";
            var br = document.createElement("br");
            var caixa = document.createElement("input");
            br.appendChild(caixa);
            caixa.type = "checkbox";
            caixa.name = valoresFacetas[i];
            caixa.value = valoresFacetas[i];
            caixa.id = valoresFacetas[i];
            caixa.onclick = function () {
                requestMinFacetaCont("preço", "tipo_de_anúncio", "aluguer");
                requestMaxFacetaCont("preço", "tipo_de_anúncio", "aluguer");
                requestMinFacetaCont("área", "tipo_de_anúncio", "aluguer");
                requestMaxFacetaCont("área", "tipo_de_anúncio", "aluguer");
                mostrarAnuncios();
            };
            var item = document.createTextNode(camelCase(valoresFacetas[i], "_"));
            paragr2.appendChild(caixa);
            paragr2.appendChild(item);
            paragr2.appendChild(br);
        }
    }
    div0.appendChild(paragr2);
    document.getElementById("div1").appendChild(div0);
}


function criarSlider(faceta) {
    var div01 = document.createElement("div");
    div01.className = "slider";
    var paragr = document.createElement("p");
    paragr.onclick = function () {
        var paragrafos = document.getElementById("paragr2" + faceta);
        if (paragrafos !== null) {
            if (paragrafos.style.display === "block") {
                paragrafos.style.display = "none";
            } else {
                paragrafos.style.display = "block";
            }
        }
    };
    var strong = document.createElement("strong");
    var titulo = document.createTextNode(camelCase(faceta, "_"));
    strong.appendChild(titulo);
    paragr.appendChild(strong);
    div01.appendChild(paragr);
    var paragr2 = document.createElement("p");
    paragr2.id = "paragr2" + faceta;
    paragr2.style.display = "none";
    var br = document.createElement("br");
    var slider = document.createElement("input");
    br.appendChild(slider);
    slider.type = "range";
    slider.name = "slider" + faceta;
    slider.value = "5";
    slider.id = "slider" + faceta;
    slider.min = minimoGlobal;
    slider.max = maximoGlobal;
    var tminimo = document.createElement("input");
    var tmaximo = document.createElement("input");
    var tresult = document.createElement("input");
    tminimo.type = "text";
    tminimo.readOnly = "true";
    tminimo.id = "tminimo" + faceta;
    tminimo.size = "1";
    tmaximo.type = "text";
    tmaximo.readOnly = "true";
    tmaximo.id = "tmaximo" + faceta;
    tmaximo.size = "1";
    tresult.type = "text";
    tresult.readOnly = "true";
    tresult.id = "tresult" + faceta;
    tresult.size = "1";

    slider.onchange = function () {
        tresult.value = slider.value;
    };
    paragr2.appendChild(tminimo);
    paragr2.appendChild(slider);
    paragr2.appendChild(tmaximo);
    paragr2.appendChild(tresult);
    paragr2.appendChild(br);
    div01.appendChild(paragr2);
    document.getElementById("div1").appendChild(div01);
}

function mostrarAnuncios() {
    var seleccao = [];
    for (var i = 0; i < structFacetas.length; i++) {
        seleccao[i] = {};
        seleccao[i]["nome"] = structFacetas[i]["nome"];
        seleccao[i]["valoresFac"] = [];

        var valores = structFacetas[i]["valoresFac"];
        if (valores) {
            var ind = 0;
            for (var j in valores) {
                var valor = valores[j];
                if (valor) {
                    if (document.getElementById(valor).checked) {
                        seleccao[i]["valoresFac"][ind] = valor;
                        ind++;
                    }
                }
            }
        }
    }
    var queryString = "";
    for (var i = 0; i < seleccao.length; i++) {
        if (seleccao[i]["valoresFac"].length > 0) {
            queryString += seleccao[i]["nome"] + "=";
            if (seleccao[i]["valoresFac"].length === 1) {
                queryString += seleccao[i]["valoresFac"][0];
            }
            if (seleccao[i]["valoresFac"].length > 1) {
                queryString += "[";
                for (var j = 0; j < seleccao[i]["valoresFac"].length; j++) {
                    queryString += seleccao[i]["valoresFac"][j];
                    if (j !== seleccao[i]["valoresFac"].length) {
                        queryString += ",";
                    }

                }
                queryString += "]";
            }
            queryString += "&";
        }

    }
    carregarAnuncios(queryString);
}

function carregarAnuncios(querystring) {
    var xmlHttp = criarObjetoXMLHttpRequest();
    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/imoveis.php?" + querystring;
    xmlHttp.open("GET", url, true);
    xmlHttp.onreadystatechange = function () {
        processorJsonAnuncios(xmlHttp);
    };
    xmlHttp.send(null);
}

function processorJsonAnuncios(xmlHttp) {
    if (xmlHttp.readyState === 4 &&
            xmlHttp.status === 200) {
        var anuncios = JSON.parse(xmlHttp.responseText);
        criarTabela(anuncios);
    }
}

function criarTabela(anuncios) {
    var divResult = document.createElement("div");
    var noTabela = document.createElement("table");
    noTabela.border = "0";
    if (anuncios.length > 0) {
        var noTr = document.createElement("tr");
        noTr.id = "resultTabTrTh";
        for (var i = 0; i < facetas.length; i++) {
            var noTh = document.createElement("th");
            var noTexto = document.createTextNode(camelCase(facetas[i], "_"));
            noTh.appendChild(noTexto);
            noTr.appendChild(noTh);
            noTabela.appendChild(noTr);
        }
    }

    for (var j = 0; j < anuncios.length; j++) {
        var noTr2 = document.createElement("tr");
        for (var prop in anuncios[j]) {
            if (prop === 'fotos') {
                if (anuncios[j][prop].length > 0) {
                    var noTd = document.createElement("td");
                    for (var k = 0; k < anuncios[j][prop].length; k++) {
                        var img = document.createElement("img");
                        img.src = anuncios[j][prop][k];
                        img.width = "100";
                        noTd.appendChild(img);
                    }
                    noTr2.appendChild(noTd);
                }
            } else {
                var noTd = document.createElement("td");
                noTd.id = "resultTabTexto";
                var noTexto = document.createTextNode(camelCase(anuncios[j][prop]));
                noTd.appendChild(noTexto);
                noTr2.appendChild(noTd);
            }
        }
        noTabela.appendChild(noTr2);
    }
    var div = document.getElementById("div2");
    while (div.firstChild) {
        div.removeChild(div.firstChild);
    }
    divResult.appendChild(noTabela);
    document.getElementById("div2").appendChild(divResult);
}

function requestMaxFacetaCont(faceta, faceta1, valor1) {
    var resultObjetoXMLHttpRequest = criarObjetoXMLHttpRequest();
    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/maxFaceta.php?facetaCont=" + faceta + "&"
            + faceta1 + "=" + valor1; //+ "%" + facetaX + valorX;
    resultObjetoXMLHttpRequest.open("GET", url, true);
    resultObjetoXMLHttpRequest.onreadystatechange = function () {
        if (resultObjetoXMLHttpRequest.readyState === 4 &&
                resultObjetoXMLHttpRequest.status === 200) {
            var xmlDoc = resultObjetoXMLHttpRequest.responseText;
            var maximo = JSON.parse(xmlDoc);
            var maximoslider = document.getElementById("slider" + faceta);
            maximoslider.max = maximo["max"];
            var tmaximo = document.getElementById("tmaximo" + faceta);
            tmaximo.value = maximo["max"];
            //maximoGlobal = maximo["max"];
            //return maximo["max"]; 
            //alert("max: " + maximo["max"]);
        }
    };
    resultObjetoXMLHttpRequest.send(null);
}

function requestMinFacetaCont(faceta, faceta1, valor1) {
    var resultObjetoXMLHttpRequest = criarObjetoXMLHttpRequest();
    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/minFaceta.php?facetaCont=" + faceta + "&"
            + faceta1 + "=" + valor1; //+ "%" + facetaX + valorX;
    resultObjetoXMLHttpRequest.open("GET", url, true);
    resultObjetoXMLHttpRequest.onreadystatechange = function () {
        if (resultObjetoXMLHttpRequest.readyState === 4 &&
                resultObjetoXMLHttpRequest.status === 200) {
            var xmlDoc = resultObjetoXMLHttpRequest.responseText;
            var minimo = JSON.parse(xmlDoc);
            var minimoslider = document.getElementById("slider" + faceta);
            minimoslider.min = minimo["min"];
            var tminimo = document.getElementById("tminimo" + faceta);
            tminimo.value = minimo["min"];
            //minimoGlobal = minimo["min"]; 
            //return minimo["min"];
            //alert("min: " + minimo["min"]);
        }
    };

    resultObjetoXMLHttpRequest.send(null);
}

function camelCase(str, str2) {
    var arrayStrings = str.split(str2);
    for (var i = 0; i < arrayStrings.length; i++) {
        var charM = arrayStrings[i].charAt(0);

        if (arrayStrings[i] !== "de" && arrayStrings[i] !== "do"
                && arrayStrings[i] !== "da" && arrayStrings[i] !== "e") {
            charM = charM.toUpperCase();
            arrayStrings[i] = arrayStrings[i].substring(1, arrayStrings[i].length);
            arrayStrings[i] = charM + arrayStrings[i];

            if (arrayStrings[i] === "Preço") {
                arrayStrings[i] += " (€)";
            } else if (arrayStrings[i] === "Área") {
                arrayStrings[i] += " (m2)";
            }
        }

    }
    return arrayStrings.join(" ");
}
