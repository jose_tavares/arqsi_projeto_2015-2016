namespace Web_API_Client.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Versao2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Administradors", "Utilizador_UtilizadorId", "dbo.Utilizadors");
            DropForeignKey("dbo.Alertas", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.Parametroes", "AlertaId", "dbo.Alertas");
            DropIndex("dbo.Administradors", new[] { "Utilizador_UtilizadorId" });
            DropIndex("dbo.Alertas", new[] { "ClienteId" });
            DropIndex("dbo.Parametroes", new[] { "AlertaId" });
            DropColumn("dbo.Anuncios", "Discriminator");
            DropTable("dbo.Administradors");
            DropTable("dbo.Alertas");
            DropTable("dbo.Parametroes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Parametroes",
                c => new
                    {
                        ParametroId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Preco = c.Int(),
                        Area = c.Int(),
                        AlertaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ParametroId);
            
            CreateTable(
                "dbo.Alertas",
                c => new
                    {
                        AlertaId = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(nullable: false),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.AlertaId);
            
            CreateTable(
                "dbo.Administradors",
                c => new
                    {
                        AdministradorId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        UtilizadorId = c.String(),
                        Utilizador_UtilizadorId = c.Int(),
                    })
                .PrimaryKey(t => t.AdministradorId);
            
            AddColumn("dbo.Anuncios", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Parametroes", "AlertaId");
            CreateIndex("dbo.Alertas", "ClienteId");
            CreateIndex("dbo.Administradors", "Utilizador_UtilizadorId");
            AddForeignKey("dbo.Parametroes", "AlertaId", "dbo.Alertas", "AlertaId", cascadeDelete: true);
            AddForeignKey("dbo.Alertas", "ClienteId", "dbo.Clientes", "ClienteId", cascadeDelete: true);
            AddForeignKey("dbo.Administradors", "Utilizador_UtilizadorId", "dbo.Utilizadors", "UtilizadorId");
        }
    }
}
