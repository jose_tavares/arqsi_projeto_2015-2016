﻿using System.Web;
using System.Web.Mvc;

namespace Web_API_Client
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            // novo, referindo a classe criada no passo anterior
            //filters.Add(new ARQSI_It2_vs01.Filters.RequireHttpsAttribute());
        }
    }
}
