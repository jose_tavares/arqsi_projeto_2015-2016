﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API_Client.Models;

namespace Web_API_Client.Controllers
{
    public class AnuncioController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Anuncio
        public IQueryable<Anuncio> GetAnuncios()
        {
            return db.Anuncios;
        }

        // GET: api/Anuncio/5
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> GetAnuncio(int id)
        {
            Anuncio anuncio = await db.Anuncios.FindAsync(id);
            if (anuncio == null)
            {
                return NotFound();
            }

            return Ok(anuncio);
        }

        // PUT: api/Anuncio/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAnuncio(int id, Anuncio anuncio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != anuncio.AnuncioId)
            {
                return BadRequest();
            }

            db.Entry(anuncio).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnuncioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Anuncio
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> PostAnuncio(Anuncio anuncio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Anuncios.Add(anuncio);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = anuncio.AnuncioId }, anuncio);
        }

        // DELETE: api/Anuncio/5
        [ResponseType(typeof(Anuncio))]
        public async Task<IHttpActionResult> DeleteAnuncio(int id)
        {
            Anuncio anuncio = await db.Anuncios.FindAsync(id);
            if (anuncio == null)
            {
                return NotFound();
            }

            db.Anuncios.Remove(anuncio);
            await db.SaveChangesAsync();

            return Ok(anuncio);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AnuncioExists(int id)
        {
            return db.Anuncios.Count(e => e.AnuncioId == id) > 0;
        }
    }
}