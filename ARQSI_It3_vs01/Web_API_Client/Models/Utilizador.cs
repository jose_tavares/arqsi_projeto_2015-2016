﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_API_Client.Models
{
    public class Utilizador
    {
        public int UtilizadorId { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string PalavraPasse { get; set; }
    }
}