﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_API_Client.Models
{
    public class Tipo_de_Anuncio
    {
        public int Tipo_de_AnuncioId { get; set; }
        public string Nome { get; set; }
    }
}