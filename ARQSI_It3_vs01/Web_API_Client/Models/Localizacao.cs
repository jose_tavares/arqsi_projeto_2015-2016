﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_API_Client.Models
{
    public class Localizacao
    {
        public int LocalizacaoId { get; set; }
        public string Nome { get; set; }
    }
}