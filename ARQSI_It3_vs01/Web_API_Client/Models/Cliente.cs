﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_API_Client.Models
{
    public class Cliente
    {
        public int ClienteId { get; set; }
        public string Nome { get; set; }
        public Localizacao Localizacao { get; set; }
        public string UtilizadorId { get; set; }
        public Utilizador Utilizador { get; set; }
    }
}