﻿using ARQSI_It2_vs01.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ARQSI_It2_vs01.Controllers
{
    public class ParametroController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Parametroes
        public ActionResult Index()
        {
            var parametros = db.Parametros.Include(p => p.Alerta);
            return View(parametros.ToList());
        }

        // GET: Parametroes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parametro parametro = db.Parametros.Find(id);
            if (parametro == null)
            {
                return HttpNotFound();
            }
            return View(parametro);
        }

        // GET: Parametroes/Create
        public ActionResult Create()
        {
            ViewBag.AlertaId = new SelectList(db.Alertas, "AlertaId", "Nome");
            return View();
        }

        // POST: Parametroes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ParametroId,Nome,Preco,Area,AlertaId")] Parametro parametro)
        {
            if (ModelState.IsValid)
            {
                db.Parametros.Add(parametro);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AlertaId = new SelectList(db.Alertas, "AlertaId", "Nome", parametro.AlertaId);
            return View(parametro);
        }

        // GET: Parametroes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parametro parametro = db.Parametros.Find(id);
            if (parametro == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlertaId = new SelectList(db.Alertas, "AlertaId", "Nome", parametro.AlertaId);
            return View(parametro);
        }

        // POST: Parametroes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ParametroId,Nome,Preco,Area,AlertaId")] Parametro parametro)
        {
            if (ModelState.IsValid)
            {
                db.Entry(parametro).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AlertaId = new SelectList(db.Alertas, "AlertaId", "Nome", parametro.AlertaId);
            return View(parametro);
        }

        // GET: Parametroes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parametro parametro = db.Parametros.Find(id);
            if (parametro == null)
            {
                return HttpNotFound();
            }
            return View(parametro);
        }

        // POST: Parametroes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Parametro parametro = db.Parametros.Find(id);
            db.Parametros.Remove(parametro);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
