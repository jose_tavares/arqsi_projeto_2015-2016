﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace ARQSI_It2_vs01.Controllers
{
    public class HomeController : Controller
    {
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Index()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Sua pagina de descricao da aplicacao.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Seu contato pagina.";

            return View();
        }
    }
}