﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ARQSI_It2_vs01.Models;

namespace ARQSI_It2_vs01.Controllers
{
    public class ImovelController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Imovel
        public ActionResult Index()
        {
            var imoveis = db.Imoveis.Include(i => i.Localizacao);
            return View(imoveis.ToList());
        }

        // GET: Imovel/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imoveis.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            return View(imovel);
        }

        // GET: Imovel/Create
        public ActionResult Create()
        {
            ViewBag.LocalizacaoId = new SelectList(db.Localizacoes, "LocalizacaoId", "Nome");
            return View();
        }

        // POST: Imovel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ImovelId,Area,LocalizacaoId")] Imovel imovel)
        {
            if (ModelState.IsValid)
            {
                db.Imoveis.Add(imovel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocalizacaoId = new SelectList(db.Localizacoes, "LocalizacaoId", "Nome", imovel.LocalizacaoId);
            return View(imovel);
        }

        // GET: Imovel/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imoveis.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalizacaoId = new SelectList(db.Localizacoes, "LocalizacaoId", "Nome", imovel.LocalizacaoId);
            return View(imovel);
        }

        // POST: Imovel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ImovelId,Area,LocalizacaoId")] Imovel imovel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(imovel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocalizacaoId = new SelectList(db.Localizacoes, "LocalizacaoId", "Nome", imovel.LocalizacaoId);
            return View(imovel);
        }

        // GET: Imovel/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imoveis.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            return View(imovel);
        }

        // POST: Imovel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Imovel imovel = db.Imoveis.Find(id);
            db.Imoveis.Remove(imovel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult CarregarImagem(string nome, HttpPostedFileBase imagem)
        {
            string fullPath = Request.MapPath("~/img/" + nome);

            if (imagem != null)
                imagem.SaveAs(fullPath);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApagarImagem(string nomeImagem)
        {
            string fullPath = Request.MapPath("~/img/" + nomeImagem);

            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath); ;
            }
            return RedirectToAction("Index");
        }
    }
}
