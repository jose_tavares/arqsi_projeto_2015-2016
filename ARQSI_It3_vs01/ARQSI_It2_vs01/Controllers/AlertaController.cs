﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ARQSI_It2_vs01.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System;

namespace ARQSI_It2_vs01.Controllers
{
    public class AlertaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Alerta
        public ActionResult Index()
        {
            try
            {
                var roleStore = new RoleStore<IdentityRole>(db);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var userStore = new UserStore<ApplicationUser>(db);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var role = roleManager.FindByName("Cliente");
                //
                if (role == null)
                {
                    // aqui sai em erro para pagina a criar
                    return Content("Nao existe roles de Cliente neste Sistema!");
                }
                else
                {

                    var utilizador = userManager.FindByName(User.Identity.Name);

                    if (utilizador != null)
                    {
                        var rolesUtilizador = userManager.GetRoles(utilizador.Id);
                        if (rolesUtilizador == null || !rolesUtilizador.Contains("Cliente"))
                        {
                            // aqui sai em erro
                            return Content("Utilizador nao tem acesso de Cliente!");
                        }
                    }
                    else
                    {
                        return Content("Utilizador inexistente!");
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var stringBuilder = new System.Text.StringBuilder();
                foreach (var erros in ex.EntityValidationErrors)
                {
                    stringBuilder.AppendFormat("{0} erro de validacao", erros.Entry.Entity.GetType());
                    foreach (var erro in erros.ValidationErrors)
                    {
                        stringBuilder.AppendFormat("- {0} : {1}", erro.PropertyName, erro.ErrorMessage);
                        stringBuilder.AppendLine();
                    }
                }

                throw new Exception(stringBuilder.ToString());
            }


            var alertas = db.Alertas.Include(a => a.Cliente);
            return View(alertas.ToList());
        }

        // GET: Alerta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // GET: Alerta/Create
        public ActionResult Create()
        {
            ViewBag.ClienteId = new SelectList(db.Clientes, "ClienteId", "Nome");
            return View();
        }

        // POST: Alerta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AlertaId,ClienteId,Nome")] Alerta alerta)
        {
            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var utilizador = userManager.FindByName(User.Identity.Name);
            var cliente = db.Clientes.Single(c => c.UtilizadorId == utilizador.Id);

            if (ModelState.IsValid)
            {
                alerta.ClienteId = cliente.ClienteId;
                db.Alertas.Add(alerta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(alerta);
        }

        // GET: Alerta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClienteId = new SelectList(db.Clientes, "ClienteId", "Nome", alerta.ClienteId);
            return View(alerta);
        }

        // POST: Alerta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AlertaId,ClienteId,Nome")] Alerta alerta)
        {
            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var utilizador = userManager.FindByName(User.Identity.Name);
            var cliente = db.Clientes.Single(c => c.UtilizadorId == utilizador.Id);

            if (ModelState.IsValid)
            {
                alerta.ClienteId = cliente.ClienteId;
                db.Entry(alerta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(alerta);
        }

        // GET: Alerta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // POST: Alerta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alerta alerta = db.Alertas.Find(id);
            db.Alertas.Remove(alerta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
