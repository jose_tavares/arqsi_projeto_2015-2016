﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI_It2_vs01.Models
{
    public class Alerta
    {
        public int AlertaId { get; set; }
        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }
        public string Nome { get; set; }
        public ICollection<Parametro> Parametros { get; set; }
    }
}