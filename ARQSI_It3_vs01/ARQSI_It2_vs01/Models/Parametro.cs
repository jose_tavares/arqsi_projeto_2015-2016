﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI_It2_vs01.Models
{
    public class Parametro
    {
        public int ParametroId { get; set; }
        public string Nome { get; set; }
        public int? Preco { get; set; }
        public int? Area { get; set; }
        public int AlertaId { get; set; }
        public virtual Alerta Alerta { get; set; }
    }
}