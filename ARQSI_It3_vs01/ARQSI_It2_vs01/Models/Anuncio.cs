﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI_It2_vs01.Models
{
    public class Anuncio
    {
        public int AnuncioId { get; set; }
        public string Nome { get; set; }

        public int ImovelId { get; set; }
        public virtual Imovel Imovel { get; set; }

        public int Tipo_de_ImovelId { get; set; }
        public virtual Tipo_de_Imovel Tipo_de_Imovel { get; set; }

        public int Preco { get; set; }
        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }

        public int Tipo_de_AnuncioId { get; set; }
        public virtual Tipo_de_Anuncio Tipo_de_Anuncio { get; set; }

        public bool Validado { get; set; }
    }
}