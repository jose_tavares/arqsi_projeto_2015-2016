﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI_It2_vs01.Models
{
    public class Tipo_de_Anuncio
    {
        public int Tipo_de_AnuncioId { get; set; }
        public string Nome { get; set; }
    }
}