namespace ARQSI_It2_vs01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Versao1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Administradors",
                c => new
                    {
                        AdministradorId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        UtilizadorId = c.String(),
                        Utilizador_UtilizadorId = c.Int(),
                    })
                .PrimaryKey(t => t.AdministradorId)
                .ForeignKey("dbo.Utilizadors", t => t.Utilizador_UtilizadorId)
                .Index(t => t.Utilizador_UtilizadorId);
            
            CreateTable(
                "dbo.Utilizadors",
                c => new
                    {
                        UtilizadorId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Email = c.String(),
                        PalavraPasse = c.String(),
                    })
                .PrimaryKey(t => t.UtilizadorId);
            
            CreateTable(
                "dbo.Alertas",
                c => new
                    {
                        AlertaId = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(nullable: false),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.AlertaId)
                .ForeignKey("dbo.Clientes", t => t.ClienteId, cascadeDelete: true)
                .Index(t => t.ClienteId);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        ClienteId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        UtilizadorId = c.String(),
                        Localizacao_LocalizacaoId = c.Int(),
                        Utilizador_UtilizadorId = c.Int(),
                    })
                .PrimaryKey(t => t.ClienteId)
                .ForeignKey("dbo.Localizacaos", t => t.Localizacao_LocalizacaoId)
                .ForeignKey("dbo.Utilizadors", t => t.Utilizador_UtilizadorId)
                .Index(t => t.Localizacao_LocalizacaoId)
                .Index(t => t.Utilizador_UtilizadorId);
            
            CreateTable(
                "dbo.Localizacaos",
                c => new
                    {
                        LocalizacaoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.LocalizacaoId);
            
            CreateTable(
                "dbo.Parametroes",
                c => new
                    {
                        ParametroId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Alerta_AlertaId = c.Int(),
                    })
                .PrimaryKey(t => t.ParametroId)
                .ForeignKey("dbo.Alertas", t => t.Alerta_AlertaId)
                .Index(t => t.Alerta_AlertaId);
            
            CreateTable(
                "dbo.Anuncios",
                c => new
                    {
                        AnuncioId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        ImovelId = c.Int(nullable: false),
                        Preco = c.Int(nullable: false),
                        ClienteId = c.Int(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.AnuncioId)
                .ForeignKey("dbo.Clientes", t => t.ClienteId, cascadeDelete: true)
                .ForeignKey("dbo.Imovels", t => t.ImovelId, cascadeDelete: true)
                .Index(t => t.ImovelId)
                .Index(t => t.ClienteId);
            
            CreateTable(
                "dbo.Imovels",
                c => new
                    {
                        ImovelId = c.Int(nullable: false, identity: true),
                        Area = c.Int(nullable: false),
                        LocalizacaoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ImovelId)
                .ForeignKey("dbo.Localizacaos", t => t.LocalizacaoId, cascadeDelete: true)
                .Index(t => t.LocalizacaoId);
            
            CreateTable(
                "dbo.Tipo_de_Imovel",
                c => new
                    {
                        Tipo_de_ImovelId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        ImovelId = c.Int(),
                    })
                .PrimaryKey(t => t.Tipo_de_ImovelId)
                .ForeignKey("dbo.Imovels", t => t.ImovelId)
                .Index(t => t.ImovelId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tipo_de_Imovel", "ImovelId", "dbo.Imovels");
            DropForeignKey("dbo.Anuncios", "ImovelId", "dbo.Imovels");
            DropForeignKey("dbo.Imovels", "LocalizacaoId", "dbo.Localizacaos");
            DropForeignKey("dbo.Anuncios", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.Parametroes", "Alerta_AlertaId", "dbo.Alertas");
            DropForeignKey("dbo.Alertas", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.Clientes", "Utilizador_UtilizadorId", "dbo.Utilizadors");
            DropForeignKey("dbo.Clientes", "Localizacao_LocalizacaoId", "dbo.Localizacaos");
            DropForeignKey("dbo.Administradors", "Utilizador_UtilizadorId", "dbo.Utilizadors");
            DropIndex("dbo.Tipo_de_Imovel", new[] { "ImovelId" });
            DropIndex("dbo.Imovels", new[] { "LocalizacaoId" });
            DropIndex("dbo.Anuncios", new[] { "ClienteId" });
            DropIndex("dbo.Anuncios", new[] { "ImovelId" });
            DropIndex("dbo.Parametroes", new[] { "Alerta_AlertaId" });
            DropIndex("dbo.Clientes", new[] { "Utilizador_UtilizadorId" });
            DropIndex("dbo.Clientes", new[] { "Localizacao_LocalizacaoId" });
            DropIndex("dbo.Alertas", new[] { "ClienteId" });
            DropIndex("dbo.Administradors", new[] { "Utilizador_UtilizadorId" });
            DropTable("dbo.Tipo_de_Imovel");
            DropTable("dbo.Imovels");
            DropTable("dbo.Anuncios");
            DropTable("dbo.Parametroes");
            DropTable("dbo.Localizacaos");
            DropTable("dbo.Clientes");
            DropTable("dbo.Alertas");
            DropTable("dbo.Utilizadors");
            DropTable("dbo.Administradors");
        }
    }
}
