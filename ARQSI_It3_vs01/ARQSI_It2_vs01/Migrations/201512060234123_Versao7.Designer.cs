// <auto-generated />
namespace ARQSI_It2_vs01.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Versao7 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Versao7));
        
        string IMigrationMetadata.Id
        {
            get { return "201512060234123_Versao7"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
