namespace ARQSI_It2_vs01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Versao61 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Parametroes", "Preco", c => c.Int());
            AlterColumn("dbo.Parametroes", "Area", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Parametroes", "Area", c => c.Int(nullable: false));
            AlterColumn("dbo.Parametroes", "Preco", c => c.Int(nullable: false));
        }
    }
}
