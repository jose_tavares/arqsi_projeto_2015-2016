namespace ARQSI_It2_vs01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Versao3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tipo_de_Imovel", "Sub_Tipo_de_ImovelId", c => c.Int(nullable: false));
            AddColumn("dbo.Tipo_de_Imovel", "Sub_Tipo_de_Imovel_Tipo_de_ImovelId", c => c.Int());
            CreateIndex("dbo.Tipo_de_Imovel", "Sub_Tipo_de_Imovel_Tipo_de_ImovelId");
            AddForeignKey("dbo.Tipo_de_Imovel", "Sub_Tipo_de_Imovel_Tipo_de_ImovelId", "dbo.Tipo_de_Imovel", "Tipo_de_ImovelId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tipo_de_Imovel", "Sub_Tipo_de_Imovel_Tipo_de_ImovelId", "dbo.Tipo_de_Imovel");
            DropIndex("dbo.Tipo_de_Imovel", new[] { "Sub_Tipo_de_Imovel_Tipo_de_ImovelId" });
            DropColumn("dbo.Tipo_de_Imovel", "Sub_Tipo_de_Imovel_Tipo_de_ImovelId");
            DropColumn("dbo.Tipo_de_Imovel", "Sub_Tipo_de_ImovelId");
        }
    }
}
