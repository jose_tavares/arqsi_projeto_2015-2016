namespace ARQSI_It2_vs01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Administradors", "Utilizador_UtilizadorId", "dbo.Utilizadors");
            DropForeignKey("dbo.Clientes", "Localizacao_LocalizacaoId", "dbo.Localizacaos");
            DropForeignKey("dbo.Clientes", "Utilizador_UtilizadorId", "dbo.Utilizadors");
            DropForeignKey("dbo.Alertas", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.Parametroes", "AlertaId", "dbo.Alertas");
            DropForeignKey("dbo.Anuncios", "ClienteId", "dbo.Clientes");
            DropForeignKey("dbo.Imovels", "LocalizacaoId", "dbo.Localizacaos");
            DropForeignKey("dbo.Anuncios", "ImovelId", "dbo.Imovels");
            DropForeignKey("dbo.Anuncios", "Tipo_de_AnuncioId", "dbo.Tipo_de_Anuncio");
            DropForeignKey("dbo.Tipo_de_Imovel", "Sub_Tipo_de_Imovel_Tipo_de_ImovelId", "dbo.Tipo_de_Imovel");
            DropForeignKey("dbo.Anuncios", "Tipo_de_ImovelId", "dbo.Tipo_de_Imovel");
            DropIndex("dbo.Administradors", new[] { "Utilizador_UtilizadorId" });
            DropIndex("dbo.Alertas", new[] { "ClienteId" });
            DropIndex("dbo.Clientes", new[] { "Localizacao_LocalizacaoId" });
            DropIndex("dbo.Clientes", new[] { "Utilizador_UtilizadorId" });
            DropIndex("dbo.Parametroes", new[] { "AlertaId" });
            DropIndex("dbo.Anuncios", new[] { "ImovelId" });
            DropIndex("dbo.Anuncios", new[] { "Tipo_de_ImovelId" });
            DropIndex("dbo.Anuncios", new[] { "ClienteId" });
            DropIndex("dbo.Anuncios", new[] { "Tipo_de_AnuncioId" });
            DropIndex("dbo.Imovels", new[] { "LocalizacaoId" });
            DropIndex("dbo.Tipo_de_Imovel", new[] { "Sub_Tipo_de_Imovel_Tipo_de_ImovelId" });
            DropTable("dbo.Administradors");
            DropTable("dbo.Utilizadors");
            DropTable("dbo.Alertas");
            DropTable("dbo.Clientes");
            DropTable("dbo.Localizacaos");
            DropTable("dbo.Parametroes");
            DropTable("dbo.Anuncios");
            DropTable("dbo.Imovels");
            DropTable("dbo.Tipo_de_Anuncio");
            DropTable("dbo.Tipo_de_Imovel");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Tipo_de_Imovel",
                c => new
                    {
                        Tipo_de_ImovelId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Sub_Tipo_de_ImovelId = c.Int(),
                        Sub_Tipo_de_Imovel_Tipo_de_ImovelId = c.Int(),
                    })
                .PrimaryKey(t => t.Tipo_de_ImovelId);
            
            CreateTable(
                "dbo.Tipo_de_Anuncio",
                c => new
                    {
                        Tipo_de_AnuncioId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.Tipo_de_AnuncioId);
            
            CreateTable(
                "dbo.Imovels",
                c => new
                    {
                        ImovelId = c.Int(nullable: false, identity: true),
                        Area = c.Int(nullable: false),
                        LocalizacaoId = c.Int(nullable: false),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.ImovelId);
            
            CreateTable(
                "dbo.Anuncios",
                c => new
                    {
                        AnuncioId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        ImovelId = c.Int(nullable: false),
                        Tipo_de_ImovelId = c.Int(nullable: false),
                        Preco = c.Int(nullable: false),
                        ClienteId = c.Int(nullable: false),
                        Tipo_de_AnuncioId = c.Int(nullable: false),
                        Validado = c.Boolean(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.AnuncioId);
            
            CreateTable(
                "dbo.Parametroes",
                c => new
                    {
                        ParametroId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Preco = c.Int(),
                        Area = c.Int(),
                        AlertaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ParametroId);
            
            CreateTable(
                "dbo.Localizacaos",
                c => new
                    {
                        LocalizacaoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.LocalizacaoId);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        ClienteId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        UtilizadorId = c.String(),
                        Localizacao_LocalizacaoId = c.Int(),
                        Utilizador_UtilizadorId = c.Int(),
                    })
                .PrimaryKey(t => t.ClienteId);
            
            CreateTable(
                "dbo.Alertas",
                c => new
                    {
                        AlertaId = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(nullable: false),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.AlertaId);
            
            CreateTable(
                "dbo.Utilizadors",
                c => new
                    {
                        UtilizadorId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Email = c.String(),
                        PalavraPasse = c.String(),
                    })
                .PrimaryKey(t => t.UtilizadorId);
            
            CreateTable(
                "dbo.Administradors",
                c => new
                    {
                        AdministradorId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        UtilizadorId = c.String(),
                        Utilizador_UtilizadorId = c.Int(),
                    })
                .PrimaryKey(t => t.AdministradorId);
            
            CreateIndex("dbo.Tipo_de_Imovel", "Sub_Tipo_de_Imovel_Tipo_de_ImovelId");
            CreateIndex("dbo.Imovels", "LocalizacaoId");
            CreateIndex("dbo.Anuncios", "Tipo_de_AnuncioId");
            CreateIndex("dbo.Anuncios", "ClienteId");
            CreateIndex("dbo.Anuncios", "Tipo_de_ImovelId");
            CreateIndex("dbo.Anuncios", "ImovelId");
            CreateIndex("dbo.Parametroes", "AlertaId");
            CreateIndex("dbo.Clientes", "Utilizador_UtilizadorId");
            CreateIndex("dbo.Clientes", "Localizacao_LocalizacaoId");
            CreateIndex("dbo.Alertas", "ClienteId");
            CreateIndex("dbo.Administradors", "Utilizador_UtilizadorId");
            AddForeignKey("dbo.Anuncios", "Tipo_de_ImovelId", "dbo.Tipo_de_Imovel", "Tipo_de_ImovelId", cascadeDelete: true);
            AddForeignKey("dbo.Tipo_de_Imovel", "Sub_Tipo_de_Imovel_Tipo_de_ImovelId", "dbo.Tipo_de_Imovel", "Tipo_de_ImovelId");
            AddForeignKey("dbo.Anuncios", "Tipo_de_AnuncioId", "dbo.Tipo_de_Anuncio", "Tipo_de_AnuncioId", cascadeDelete: true);
            AddForeignKey("dbo.Anuncios", "ImovelId", "dbo.Imovels", "ImovelId", cascadeDelete: true);
            AddForeignKey("dbo.Imovels", "LocalizacaoId", "dbo.Localizacaos", "LocalizacaoId", cascadeDelete: true);
            AddForeignKey("dbo.Anuncios", "ClienteId", "dbo.Clientes", "ClienteId", cascadeDelete: true);
            AddForeignKey("dbo.Parametroes", "AlertaId", "dbo.Alertas", "AlertaId", cascadeDelete: true);
            AddForeignKey("dbo.Alertas", "ClienteId", "dbo.Clientes", "ClienteId", cascadeDelete: true);
            AddForeignKey("dbo.Clientes", "Utilizador_UtilizadorId", "dbo.Utilizadors", "UtilizadorId");
            AddForeignKey("dbo.Clientes", "Localizacao_LocalizacaoId", "dbo.Localizacaos", "LocalizacaoId");
            AddForeignKey("dbo.Administradors", "Utilizador_UtilizadorId", "dbo.Utilizadors", "UtilizadorId");
        }
    }
}
