namespace ARQSI_It2_vs01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Versao7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Anuncios", "Validado", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Anuncios", "Validado");
        }
    }
}
