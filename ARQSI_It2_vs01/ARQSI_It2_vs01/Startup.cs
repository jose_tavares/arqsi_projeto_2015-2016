﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ARQSI_It2_vs01.Startup))]
namespace ARQSI_It2_vs01
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
