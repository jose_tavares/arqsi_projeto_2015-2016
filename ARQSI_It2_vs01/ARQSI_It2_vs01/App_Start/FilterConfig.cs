﻿using System.Web;
using System.Web.Mvc;

namespace ARQSI_It2_vs01
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
