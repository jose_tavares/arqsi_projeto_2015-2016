﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI_It2_vs01.Models
{
    public class Administrador
    {
        public int AdministradorId { get; set; }
        public string Nome { get; set; }
        public string UtilizadorId { get; set; }
        public virtual Utilizador Utilizador { get; set; }
    }
}