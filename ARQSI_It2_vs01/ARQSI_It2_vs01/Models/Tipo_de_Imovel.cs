﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI_It2_vs01.Models
{
    public class Tipo_de_Imovel
    {
        public int Tipo_de_ImovelId { get; set; }
        public string Nome { get; set; }
        public int? Sub_Tipo_de_ImovelId { get; set; }
        public virtual Tipo_de_Imovel Sub_Tipo_de_Imovel { get; set; }
    }
}