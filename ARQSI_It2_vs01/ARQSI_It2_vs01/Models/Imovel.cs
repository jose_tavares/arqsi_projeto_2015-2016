﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI_It2_vs01.Models
{
    public class Imovel
    {
        public int ImovelId { get; set; }
        public int Area { get; set; }
        public int LocalizacaoId { get; set; }
        public virtual Localizacao Localizacao { get; set; }
        public string Nome { get; set; }
        public string Imagem { get { return Nome.Replace(" ", string.Empty) + ".jpg"; } }
    }
}