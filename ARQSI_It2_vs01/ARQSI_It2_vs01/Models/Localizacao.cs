﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARQSI_It2_vs01.Models
{
    public class Localizacao
    {
        public int LocalizacaoId { get; set; }
        public string Nome { get; set; }
    }
}