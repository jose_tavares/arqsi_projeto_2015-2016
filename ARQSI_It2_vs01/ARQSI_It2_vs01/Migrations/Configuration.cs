namespace ARQSI_It2_vs01.Migrations
{
    using Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ARQSI_It2_vs01.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ARQSI_It2_vs01.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            try
            {

                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                var role = roleManager.FindByName("Admin");
                if (role == null)
                {
                    var roleNew = new IdentityRole();
                    roleNew.Name = "Admin";
                    var result = roleManager.Create(roleNew);
                }

                string UserNameTmp = "Admin@mail.com";
                var passwordHash = new PasswordHasher();
                string password = passwordHash.HashPassword("Qwerty123.");
                context.Users.AddOrUpdate(u => u.UserName,
                    new ApplicationUser
                    {
                        UserName = UserNameTmp,
                        PasswordHash = password,
                        PhoneNumber = "+351221111111",
                        Email = UserNameTmp,
                        SecurityStamp = Guid.NewGuid().ToString()

                    });
                context.SaveChanges();

                var example = context.Users.Single(g => g.UserName == UserNameTmp);
                string userIdNew = example.Id;

                context.Utilizadores.AddOrUpdate(x => x.UtilizadorId,
                new Utilizador() { Nome = userIdNew, PalavraPasse = password, Email = UserNameTmp });

                Console.WriteLine("userIdNew: " + userIdNew);
                context.Administradores.AddOrUpdate(x => x.Nome,
                new Administrador() { UtilizadorId = userIdNew, Nome = UserNameTmp });

                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                userManager.AddToRole(userIdNew, "Admin");

                context.SaveChanges();

                context.Localizacoes.AddOrUpdate(x => x.LocalizacaoId,
                    new Localizacao() { Nome = "Rua numero 1" });
                context.Localizacoes.AddOrUpdate(x => x.LocalizacaoId,
                    new Localizacao() { Nome = "Rua numero 2" });
                context.Localizacoes.AddOrUpdate(x => x.LocalizacaoId,
                    new Localizacao() { Nome = "Rua numero 3" });

                context.SaveChanges();

                context.Imoveis.AddOrUpdate(x => x.ImovelId,
                    new Imovel() { Area = 100, Nome = "Rua numero 1", LocalizacaoId = 1 });
                context.Imoveis.AddOrUpdate(x => x.ImovelId,
                    new Imovel() { Area = 120, Nome = "Rua numero 2" ,LocalizacaoId = 2 });
                context.Imoveis.AddOrUpdate(x => x.ImovelId,
                    new Imovel() { Area = 150, Nome = "Rua numero 3" ,LocalizacaoId = 3 });

                context.SaveChanges();

                var tipo_de_anuncio = context.Tipo_de_Anuncio.FirstOrDefault();
                if (tipo_de_anuncio == null)
                {
                    context.Tipo_de_Anuncio.AddOrUpdate(x => x.Tipo_de_AnuncioId,
                    new Tipo_de_Anuncio() { Nome = "Venda" });

                    context.Tipo_de_Anuncio.AddOrUpdate(x => x.Tipo_de_AnuncioId,
                    new Tipo_de_Anuncio() { Nome = "Compra" });

                    context.Tipo_de_Anuncio.AddOrUpdate(x => x.Tipo_de_AnuncioId,
                    new Tipo_de_Anuncio() { Nome = "Aluguer" });


                    context.Tipo_de_Anuncio.AddOrUpdate(x => x.Tipo_de_AnuncioId,
                    new Tipo_de_Anuncio() { Nome = "Permuta" });

                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var sb = new System.Text.StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new Exception(sb.ToString());
            }

        }
    }
}
