namespace ARQSI_It2_vs01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Versao51 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Imovels", "Nome", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Imovels", "Nome");
        }
    }
}
