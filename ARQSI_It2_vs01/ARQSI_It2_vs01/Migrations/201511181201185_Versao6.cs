namespace ARQSI_It2_vs01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Versao6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tipo_de_Imovel", "ImovelId", "dbo.Imovels");
            DropIndex("dbo.Tipo_de_Imovel", new[] { "ImovelId" });
            DropColumn("dbo.Tipo_de_Imovel", "ImovelId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tipo_de_Imovel", "ImovelId", c => c.Int());
            CreateIndex("dbo.Tipo_de_Imovel", "ImovelId");
            AddForeignKey("dbo.Tipo_de_Imovel", "ImovelId", "dbo.Imovels", "ImovelId");
        }
    }
}
