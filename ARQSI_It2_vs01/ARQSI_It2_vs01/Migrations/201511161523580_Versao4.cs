namespace ARQSI_It2_vs01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Versao4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Parametroes", "Alerta_AlertaId", "dbo.Alertas");
            DropIndex("dbo.Parametroes", new[] { "Alerta_AlertaId" });
            RenameColumn(table: "dbo.Parametroes", name: "Alerta_AlertaId", newName: "AlertaId");
            CreateTable(
                "dbo.Tipo_de_Anuncio",
                c => new
                    {
                        Tipo_de_AnuncioId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.Tipo_de_AnuncioId);
            
            AddColumn("dbo.Parametroes", "Preco", c => c.Int(nullable: false));
            AddColumn("dbo.Parametroes", "Area", c => c.Int(nullable: false));
            AddColumn("dbo.Anuncios", "Tipo_de_ImovelId", c => c.Int(nullable: false));
            AddColumn("dbo.Anuncios", "Tipo_de_AnuncioId", c => c.Int(nullable: false));
            AlterColumn("dbo.Parametroes", "AlertaId", c => c.Int(nullable: false));
            AlterColumn("dbo.Tipo_de_Imovel", "Sub_Tipo_de_ImovelId", c => c.Int());
            CreateIndex("dbo.Parametroes", "AlertaId");
            CreateIndex("dbo.Anuncios", "Tipo_de_ImovelId");
            CreateIndex("dbo.Anuncios", "Tipo_de_AnuncioId");
            AddForeignKey("dbo.Anuncios", "Tipo_de_AnuncioId", "dbo.Tipo_de_Anuncio", "Tipo_de_AnuncioId", cascadeDelete: true);
            AddForeignKey("dbo.Anuncios", "Tipo_de_ImovelId", "dbo.Tipo_de_Imovel", "Tipo_de_ImovelId", cascadeDelete: true);
            AddForeignKey("dbo.Parametroes", "AlertaId", "dbo.Alertas", "AlertaId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Parametroes", "AlertaId", "dbo.Alertas");
            DropForeignKey("dbo.Anuncios", "Tipo_de_ImovelId", "dbo.Tipo_de_Imovel");
            DropForeignKey("dbo.Anuncios", "Tipo_de_AnuncioId", "dbo.Tipo_de_Anuncio");
            DropIndex("dbo.Anuncios", new[] { "Tipo_de_AnuncioId" });
            DropIndex("dbo.Anuncios", new[] { "Tipo_de_ImovelId" });
            DropIndex("dbo.Parametroes", new[] { "AlertaId" });
            AlterColumn("dbo.Tipo_de_Imovel", "Sub_Tipo_de_ImovelId", c => c.Int(nullable: false));
            AlterColumn("dbo.Parametroes", "AlertaId", c => c.Int());
            DropColumn("dbo.Anuncios", "Tipo_de_AnuncioId");
            DropColumn("dbo.Anuncios", "Tipo_de_ImovelId");
            DropColumn("dbo.Parametroes", "Area");
            DropColumn("dbo.Parametroes", "Preco");
            DropTable("dbo.Tipo_de_Anuncio");
            RenameColumn(table: "dbo.Parametroes", name: "AlertaId", newName: "Alerta_AlertaId");
            CreateIndex("dbo.Parametroes", "Alerta_AlertaId");
            AddForeignKey("dbo.Parametroes", "Alerta_AlertaId", "dbo.Alertas", "AlertaId");
        }
    }
}
