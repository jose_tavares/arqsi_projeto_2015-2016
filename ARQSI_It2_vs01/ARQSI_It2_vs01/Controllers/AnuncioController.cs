﻿using ARQSI_It2_vs01.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ARQSI_It2_vs01.Controllers
{
    public class AnuncioController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Anuncio
        public ActionResult Index()
        {
            try
            {
                var roleStore = new RoleStore<IdentityRole>(db);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var userStore = new UserStore<ApplicationUser>(db);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var role = roleManager.FindByName("Cliente");

                if(role == null)
                {
                    // retorna erro para pagina a criar
                    return Content("Nao existe roles de Cliente neste Sistema!");
                }
                else
                {
                    var utilizador = userManager.FindByName(User.Identity.Name);
                    if(utilizador != null)
                    {
                        var rolesUtilizador = userManager.GetRoles(utilizador.Id);
                        if(rolesUtilizador == null || !rolesUtilizador.Contains("Cliente"))
                        {
                            //retorna em erro
                            return Content("Utilizador nao tem acesso de Cliente!");
                        }
                    }
                    else
                    {
                        return Content("Utilizador inexistente!");
                    }
                }
            }
            catch(System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var stringBuilder = new System.Text.StringBuilder();
                foreach(var erros in ex.EntityValidationErrors)
                {
                    stringBuilder.AppendFormat("{0} erro de validacao", erros.Entry.Entity.GetType());
                    foreach (var erro in erros.ValidationErrors)
                    {
                        stringBuilder.AppendFormat("- {0} : {1}", erro.PropertyName, erro.ErrorMessage);
                        stringBuilder.AppendLine();
                    }
                }
                throw new Exception(stringBuilder.ToString());
            }

            var anuncios = db.Anuncios.Include(a => a.Tipo_de_Anuncio).Include(a => a.Cliente).Include(a => a.Imovel).Include(a => a.Tipo_de_Imovel);
            return View(anuncios.ToList());
        }

        // GET: Anuncio/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }
            return View(anuncio);
        }

        // GET: Anuncio/Create
        public ActionResult Create()
        {
            ViewBag.Tipo_de_AnuncioId = new SelectList(db.Tipo_de_Anuncio, "Tipo_de_AnuncioId", "Nome");

            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var utilizador = userManager.FindByName(User.Identity.Name);
            var list = db.Clientes.Where(a => a.UtilizadorId == utilizador.Id);

            var selectCliente = new SelectList(list, "ClienteId", "Nome");
            ViewBag.ClienteId = selectCliente;

            ViewBag.ImovelId = new SelectList(db.Imoveis, "ImovelId", "Nome");
            ViewBag.Tipo_de_ImovelId = new SelectList(db.Tipo_de_Imovel, "Tipo_de_ImovelId", "Nome");
            return View();
        }

        // POST: Anuncio/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnuncioId,Nome,ImovelId,Preco,ClienteId,Tipo_de_ImovelId,Tipo_de_AnuncioId")] Anuncio anuncio)
        {
            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var utilizador = userManager.FindByName(User.Identity.Name);
            var cliente = db.Clientes.Single(c => c.UtilizadorId == utilizador.Id);

            if (ModelState.IsValid)
            {
                anuncio.ClienteId = cliente.ClienteId;
                db.Anuncios.Add(anuncio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Tipo_de_AnuncioId = new SelectList(db.Tipo_de_Anuncio, "Tipo_de_AnuncioId", "Nome",anuncio.Tipo_de_AnuncioId);
            var list = db.Clientes.Where(a => a.UtilizadorId == utilizador.Id);
            var selectClient = new SelectList(list, "ClienteId", "Nome");
            ViewBag.clientId = selectClient;

            ViewBag.ImovelId = new SelectList(db.Imoveis, "ImovelId", "Nome", anuncio.ImovelId);
            ViewBag.Tipo_de_ImovelId = new SelectList(db.Tipo_de_Imovel, "Tipo_de_ImovelId", "Nome", anuncio.Tipo_de_ImovelId);
            return View(anuncio);
        }

        // GET: Anuncio/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }

            ViewBag.Tipo_de_AnuncioId = new SelectList(db.Tipo_de_Anuncio, "Tipo_de_AnuncioId", "Nome", anuncio.Tipo_de_AnuncioId);

            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var utilizador = userManager.FindByName(User.Identity.Name);
            var list = db.Clientes.Where(a => a.UtilizadorId == utilizador.Id);
            var selectCliente = new SelectList(list, "ClienteId", "Nome");
            ViewBag.ClienteId = selectCliente;

            ViewBag.ImovelId = new SelectList(db.Imoveis, "ImovelId", "Nome", anuncio.ImovelId);
            ViewBag.Tipo_de_ImovelId = new SelectList(db.Tipo_de_Imovel, "Tipo_de_ImovelId", "Nome", anuncio.Tipo_de_ImovelId);
            return View(anuncio);
        }

        // POST: Anuncio/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnuncioId,Nome,ImovelId,Preco,ClienteId,Tipo_de_ImovelId,Tipo_de_AnuncioId")] Anuncio anuncio)
        {
            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var utilizador = userManager.FindByName(User.Identity.Name);
            var cliente = db.Clientes.Single(c => c.UtilizadorId == utilizador.Id);

            if (ModelState.IsValid)
            {
                anuncio.ClienteId = cliente.ClienteId;
                db.Entry(anuncio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Tipo_de_AnuncioId = new SelectList(db.Tipo_de_Anuncio, "Tipo_de_AnuncioId", "Nome", anuncio.Tipo_de_AnuncioId);
            var list = db.Clientes.Where(a => a.UtilizadorId == utilizador.Id);
            var selectCliente = new SelectList(list, "ClienteId", "Nome");
            ViewBag.ClienteId = selectCliente;
            ViewBag.ImovelId = new SelectList(db.Imoveis, "ImovelId", "Nome", anuncio.ImovelId);
            ViewBag.Tipo_de_ImovelId = new SelectList(db.Tipo_de_Imovel, "Tipo_de_ImovelId", "Nome", anuncio.Tipo_de_ImovelId);
            return View(anuncio);
        }

        // GET: Anuncio/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anuncio anuncio = db.Anuncios.Find(id);
            if (anuncio == null)
            {
                return HttpNotFound();
            }
            return View(anuncio);
        }

        // POST: Anuncio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Anuncio anuncio = db.Anuncios.Find(id);
            db.Anuncios.Remove(anuncio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
