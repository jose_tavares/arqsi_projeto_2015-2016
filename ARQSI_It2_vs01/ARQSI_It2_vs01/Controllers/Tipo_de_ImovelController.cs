﻿using ARQSI_It2_vs01.DAL;
using ARQSI_It2_vs01.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ARQSI_It2_vs01.Controllers
{
    public class Tipo_de_ImovelController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tipo_de_Imovel
        public ActionResult Index()
        {
            try
            {
                var roleStore = new RoleStore<IdentityRole>(db);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var userStore = new UserStore<ApplicationUser>(db);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var role = roleManager.FindByName("Admin");

                if (role == null)
                {
                    // retorna em erro para pagina a criar
                    return Content("Nao existe roles de Administrador no Sistema!");
                }
                else
                {
                    var utilizador = userManager.FindByName(User.Identity.Name);

                    if (utilizador != null)
                    {
                        var rolesForUser = userManager.GetRoles(utilizador.Id);
                        if (rolesForUser == null || !rolesForUser.Contains("Admin"))
                        {
                            // retorna em erro
                            return Content("Utilizador nao tem acesso a privilegios de administrador!");
                        }
                    }
                    else
                    {
                        return Content("Nao existe esse utilizador!");
                    }
                }

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var stringBuilder = new System.Text.StringBuilder();
                foreach (var erros in ex.EntityValidationErrors)
                {
                    stringBuilder.AppendFormat("{0} erro de validacao", erros.Entry.Entity.GetType());
                    foreach (var erro in erros.ValidationErrors)
                    {
                        stringBuilder.AppendFormat("- {0} : {1}", erro.PropertyName, erro.ErrorMessage);
                        stringBuilder.AppendLine();
                    }
                }

                throw new Exception(stringBuilder.ToString());
            }
            var Tipo_de_Imovel = db.Tipo_de_Imovel.Include(r => r.Sub_Tipo_de_Imovel);
            return View(Tipo_de_Imovel.ToList());
        }

        // GET: Tipo_de_Imovel/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipo_de_Imovel tipo_de_Imovel = db.Tipo_de_Imovel.Find(id);
            if (tipo_de_Imovel == null)
            {
                return HttpNotFound();
            }
            return View(tipo_de_Imovel);
        }

        // GET: Tipo_de_Imovel/Create
        public ActionResult Create()
        {
            ViewBag.Tipo_de_ImovelId = new SelectList(db.Tipo_de_Imovel, "Tipo_de_ImovelId", "Nome");
            return View();
        }

        // POST: Tipo_de_Imovel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Tipo_de_ImovelId,Nome,Sub_Tipo_de_ImovelId")] Tipo_de_Imovel tipo_de_Imovel)
        {
            if (ModelState.IsValid)
            {
                db.Tipo_de_Imovel.Add(tipo_de_Imovel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Tipo_de_ImovelId = new SelectList(db.Tipo_de_Imovel, "Tipo_de_ImovelId", "Nome", tipo_de_Imovel.Sub_Tipo_de_ImovelId);
            return View(tipo_de_Imovel);
        }

        // GET: Tipo_de_Imovel/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipo_de_Imovel tipo_de_Imovel = db.Tipo_de_Imovel.Find(id);
            if (tipo_de_Imovel == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tipo_de_ImovelId = new SelectList(db.Tipo_de_Imovel, "Tipo_de_ImovelId", "Nome", tipo_de_Imovel.Sub_Tipo_de_ImovelId);
            return View(tipo_de_Imovel);
        }

        // POST: Tipo_de_Imovel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Tipo_de_ImovelId,Nome,Sub_Tipo_de_ImovelId")] Tipo_de_Imovel tipo_de_Imovel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipo_de_Imovel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Tipo_de_ImovelId = new SelectList(db.Tipo_de_Imovel, "Tipo_de_ImovelId", "Nome", tipo_de_Imovel.Sub_Tipo_de_ImovelId);
            return View(tipo_de_Imovel);
        }

        // GET: Tipo_de_Imovel/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tipo_de_Imovel tipo_de_Imovel = db.Tipo_de_Imovel.Find(id);
            if (tipo_de_Imovel == null)
            {
                return HttpNotFound();
            }
            return View(tipo_de_Imovel);
        }

        // POST: Tipo_de_Imovel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tipo_de_Imovel tipo_de_Imovel = db.Tipo_de_Imovel.Find(id);
            db.Tipo_de_Imovel.Remove(tipo_de_Imovel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
