﻿using ARQSI_It2_vs01.Models;
using System.Data.Entity;

namespace ARQSI_It2_vs01.DAL
{
    public class ImorestDbContext:DbContext
    {
        public ImorestDbContext(): base("DefaultConnection") { }
        public DbSet<Administrador> Administradores { get; set; }
        public DbSet<Alerta> Alertas { get; set; }
        public DbSet<Anuncio> Anuncios { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Imovel> Imoveis { get; set; }
        public DbSet<Tipo_de_Imovel> Tipo_de_Imovel { get; set; }
    }
}