<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Mediador for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Mediador\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Mediador\DTO\Credenciais;
use Mediador\Form\LoginForm;
use Mediador\Form\ConfigForm;
use Mediador\Form\AnuncioForm;
use Mediador\Model\Anuncio;
use Mediador\Infrastructure\Services\ImoServices;

use Zend\Http\Request;
use Zend\Json\Json;

class MediadorController extends AbstractActionController
{
    public $anuncioTable;
    
    public function configAction()
    {
        $form = new ConfigForm();
        $form->get('submit')->setValue('Set');
    
        session_start();
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $_SESSION['server'] = $request->getPost()['server'];
            
            return $this->redirect()->toRoute('mediador', array('controller'=>'mediador', 'action' => 'login'));
        }
        else
        {
            if(!empty($_SESSION['server']))
                $form->setAttribute("Server", $_SESSION['server']);

           return array('form' => $form);
        }
    }

    public function loginAction()
    {
        $form = new LoginForm();
        $form->get('submit')->setValue('Login');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            session_start();
            ImoServices::Logout();
            
            $credenciais = new Credenciais();
            $form->setInputFilter($credenciais->getInputFilter());
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $credenciais->exchangeArray($form->getData());
               
                if( ImoServices::Login($credenciais) )

                    // Redirect to values
                    return $this->redirect()->toRoute('mediador', array('controller'=>'mediador', 'action' => 'values'));
            }
        }
        return array('form' => $form);
    }

    public function logoutAction()
    {
        ImoServices::Logout();

        return $this->redirect()->toRoute('mediador', array('controller'=>'mediador', 'action' => 'login'));
    }
    
    public function valuesAction()
    {
        $body = ImoServices::getValues();

        return new ViewModel(array(
            'values' => Json::decode($body)
        ));
    }
    
    
    public function indexAction()
    {
        return new ViewModel(array(
             'anuncios' => $this->getAnunciosTable()->fetchAll(),
         ));
    }

     public function addAction()
     {
         $form = new AnuncioForm();
         $form->get('submit')->setValue('Add');

         $request = $this->getRequest();
         if ($request->isPost()) {
             $anuncio = new Anuncio();
             $form->setInputFilter($anuncio->getInputFilter());
             $form->setData($request->getPost());

             if ($form->isValid()) {
                 $anuncio->exchangeArray($form->getData());
                 $this->getAnuncioTable()->saveAnuncio($anuncio);

                 // Redirect to list of anuncios
                 return $this->redirect()->toRoute('mediador');
             }
         }
         return array('form' => $form);
     }

     public function editAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);
         if (!$id) {
             return $this->redirect()->toRoute('mediador', array(
                 'action' => 'add'
             ));
         }

         // Get the Anuncio with the specified id.  An exception is thrown
         // if it cannot be found, in which case go to the index page.
         try {
             $anuncio = $this->getAnuncioTable()->getAnuncio($id);
         }  
         catch (\Exception $ex) {
             return $this->redirect()->toRoute('mediador', array(
                 'action' => 'index'
             ));
         }

         $form  = new AnuncioForm();
         $form->bind($anuncio);
         $form->get('submit')->setAttribute('value', 'Edit');

         $request = $this->getRequest();
         if ($request->isPost()) {
             $form->setInputFilter($anuncio->getInputFilter());
             $form->setData($request->getPost());

             if ($form->isValid()) {
                 $this->getAnuncioTable()->saveAnuncio($anuncio);

                 // Redirect to list of anuncios
                 return $this->redirect()->toRoute('mediador');
             }
         }

         return array(
             'id' => $id,
             'form' => $form,
         );
     }

     public function deleteAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);
         if (!$id) {
             return $this->redirect()->toRoute('mediador');
         }

         $request = $this->getRequest();
         if ($request->isPost()) {
             $del = $request->getPost('del', 'No');

             if ($del == 'Yes') {
                 $id = (int) $request->getPost('id');
                 $this->getAnuncioTable()->deleteAnuncio($id);
             }

             // Redirect to list of anuncios
             return $this->redirect()->toRoute('mediador');
         }

         return array(
             'id'    => $id,
             'anuncio' => $this->getAnuncioTable()->getAnuncio($id)
         );
     }
     
    public function getAnuncioTable()
    {
        if (!$this->anuncioTable) {
            $sm = $this->getServiceLocator();
            $this->anuncioTable = $sm->get('Mediador\Infrastructure\DAL\AnuncioTable');
        }
        return $this->anuncioTable;
    }
}
