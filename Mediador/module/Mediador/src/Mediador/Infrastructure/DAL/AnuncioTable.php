<?php
 namespace Mediador\Infrastructure\DAL;

 use Zend\Db\TableGateway\TableGateway;
 use Mediador\Model\Anuncio;

 class AnuncioTable
 {
     protected $tableGateway;

     public function __construct(TableGateway $tableGateway)
     {
         $this->tableGateway = $tableGateway;
     }

     public function fetchAll()
     {
         $resultSet = $this->tableGateway->select();
         return $resultSet;
     }

     public function getAnuncio($id)
     {
         $id  = (int) $id;
         $rowset = $this->tableGateway->select(array('id' => $id));
         $row = $rowset->current();
         if (!$row) {
             throw new \Exception("Could not find row $id");
         }
         return $row;
     }

     public function saveAnuncio(Anuncio $anuncio)
     {
         $data = array(
             'nome' => $anuncio->nome,
         );

         $id = (int) $anuncio->id;
         if ($id == 0) {
             $this->tableGateway->insert($data);
         } else {
             if ($this->getAnuncio($id)) {
                 $this->tableGateway->update($data, array('id' => $id));
             } else {
                 throw new \Exception('Anuncio id does not exist');
             }
         }
     }

     public function deleteAnuncio($id)
     {
         $this->tableGateway->delete(array('id' => (int) $id));
     }
 }

?>